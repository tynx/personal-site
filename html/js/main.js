var CursorAnimation = function(sleep){
	var running = false;
	var interval = null;
	var sleepInterval = sleep;
	var speedFactor = 1;

	var windowWidth = 0;
	var windowHeight = 0;
	var cursorWidth = 8;
	var cursorHeight = 20;
	var cursorX = 0;
	var cursorY = 0;

	var overlay = null;
	var cursor = null;

	function init(){
		overlay = document.getElementById('cursor_overlay');
		cursor = document.getElementById('cursor');
	}

	function animationStep(){
		cursorX += cursorWidth * speedFactor;

		var newTop = cursorY - cursorHeight;
		var newHeight = windowHeight - newTop;

		if((cursorY + cursorHeight * 2) > windowHeight){
			finish();
			return;
		}

		if(cursorX > windowWidth){
			cursorX -= windowWidth;
			cursorY += cursorHeight;
		}

		cursor.style.top = cursorY +'px';
		cursor.style.left = cursorX + 'px';
		overlay.style.top = newTop + 'px';
		overlay.style.height = newHeight + 'px';

	}

	function finish(){
		clearInterval(interval);
		cursor.style.display = 'none';
		overlay.style.display = 'none';
		running = false;
	}

	this.start = function(){
		if(running) return;
		running = true;

		overlay.style.display = 'block';
		cursor.style.display = 'block';

		windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		windowHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
		speedFactor = windowWidth*windowHeight/20000;
		cursorX = 0;
		cursorY = 0;

		interval = setInterval(animationStep, sleepInterval);
	}

	init();
}

var Site = function(fps){
	var updateRate = 1/fps;
	var validActions = ["home", "about", "contact"];
	var oldAction = "";
	var ca = null;
	
	this.init = function(){
		document.getElementById('navi').style.display='block';
		setInterval(checkHash, updateRate);
		ca = new CursorAnimation(updateRate);
	}

	function checkHash(){
		var currentAction = window.location.hash.slice(1);
		if(currentAction == ''){
			currentAction = 'home'
		}
		
		if(currentAction != oldAction)
			setNewContent((oldAction = currentAction));

		document.getElementById('title').style.width = 'auto';
		if(document.getElementById('navi').offsetHeight > 29)
			document.getElementById('title').style.width = '100%';
	}
	
	function setNewContent(actionName){
		ca.start();
		for(var i=0, l=validActions.length; i<l; i++){
			var display = 'none';
			var className = ''
			if(actionName == validActions[i]){
				display = 'block';
				className = 'active';
			}
			document.getElementById('content_' + validActions[i]).style.display = display;
			document.getElementById('link_' + validActions[i]).className = className;
		}
	}
}

function init(){
	(new Site(25)).init();
}

if (document.readyState != 'loading'){
	init();
}else{
	document.addEventListener('DOMContentLoaded', init());
}




